<?php
  
	register_nav_menu( 'news', 'News Menu' );
	
	add_action( 'init', 'create_news_post' );
	function create_news_post() {
		register_post_type( 'News Entries',
			array(
				'labels' => array(
					'name' => __( 'News' ),
					'singular_name' => __( 'News' )
				),
			'public' => true,
			'has_archive' => true,
			)
		);
	}
	
?>
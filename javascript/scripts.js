var showModal = function() {
	var modalWidth = document.body.scrollWidth;
	var modalHeight = document.body.scrollHeight;
	$('.overlay').css({
		height: modalHeight,
		width: modalWidth
	});
	
	var contactWidth = $('.contact').innerWidth();
	var contactHeight = $('.contact').innerHeight();
	var winHeight = window.innerHeight;
	console.log(contactWidth);
	$('.contact').css({
		left: '50%',
		marginLeft: (contactWidth * -1)/2,
		top: winHeight/2,
		marginTop: (contactHeight * -1)/2
	})
}
<?php

// Exit if accessed directly
if ( !defined('ABSPATH')) exit;

/**
 * Footer Template
 *
 *
 * @file           footer.php
 * @package        RDA 
 * @author         Chad Bartels 
 * @copyright      2003 - 2013 ThemeID
 * @license        license.txt
 * @version        Release: 1.2
 * @filesource     wp-content/themes/RDA/footer.php
 * @link           http://codex.wordpress.org/Theme_Development#Footer_.28footer.php.29
 * @since          available since Release 1.0
 */
?>
    </div><!-- end of #wrapper -->
    <?php responsive_wrapper_end(); // after wrapper hook ?>
</div><!-- end of #container -->
<?php responsive_container_end(); // after container hook ?>

<div id="footer" class="clearfix">

    <div id="footer-wrapper" class="cf">
    
        <div class="grid col-940 rda">
        	<div class="grid col-300">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/rda-footer-logo.png" />
						<h6>Regional Dance America</h6>
					</div>
				</div>
				<div class="grid col-940">
					<div class="grid col-220 left">
						<p><a href="" class="contact">Contact Us</a></p>
						<p><a href="">RDADirector.com</a></p>
					</div>
					<div class="grid col-720 right">
							<?php if (has_nav_menu('footer-menu', 'responsive')) { ?>
						        <?php wp_nav_menu(array(
									    'container'       => '',
										'fallback_cb'	  =>  false,
										'menu_class'      => 'footer-menu',
										'theme_location'  => 'footer-menu')
										); 
									?>
					    <?php } ?>
					</div><!-- end of col-540 -->
					
        </div><!-- end of col-940 -->        
        
    </div><!-- end #footer-wrapper -->
    
</div><!-- end #footer -->
<div class="overlay">
	<div class="contact cf">
		<form method="post" action="<?php echo get_stylesheet_directory_uri(); ?>/contact.php">
			<h3>Contact</h3>
			
			<div>
				<label>First Name:</label>
				<input type="text" name="firstName" />
				
				<label>Company Name:</label>
				<input type="text" name="companyName" />
				
				<label>City:</label>
				<input type="text" name="city" />
			</div>
			
			<div>
				<label>Last Name:</label>
				<input type="text" name="lastName" />
			
				<label>Phone Number:</label>
				<input type="text" name="phoneNumber" />
		
				<label>State:</label>
				<input type="text" name="state" />
			</div>
			<div class="comments">
				<label>Comments:</label>
				<textarea name="comments"></textarea>
			</div>
			<div class="submit">
				<input type="submit" value="Submit" name="submit" />
			</div>
		</form>
	</div>
</div>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/javascript/scripts.js"></script>
<script>
$(function() {
	$('.contact').click(function(e) {
		e.preventDefault();
		$('.overlay').fadeIn();
	});
	
	$('.overlay').click(function() {
		if(event.target.className == "overlay") {
			$('.overlay').fadeOut();
		}
	});
	
	$('.contact form .submit input').click(function(e) {
		e.preventDefault();
		$.ajax({
			type: 'POST',
			data: $(this).closest('form').serialize(),
			url: '<?php echo get_stylesheet_directory_uri(); ?>/contact.php'
		}).done(function(data) {
			alert(data);
		})
	})
	
	$('.search-button').click(function(){
		if($('#search').is(':visible')) {
			$('#search').removeClass('open').fadeOut(100);
		}else{
			$('#search').addClass('open').fadeIn(100);
		}
	});
});	
</script>
<?php wp_footer(); ?>
</body>
</html>
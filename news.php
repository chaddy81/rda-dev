<?php

// Exit if accessed directly
if ( !defined('ABSPATH')) exit;

/**
 * Home Widgets Template
 *
 * Template Name: News
 *
 * @file           news.php
 * @package        RDA 
 * @author         Chad Bartels
 * @copyright      2003 - 2013 ThemeID
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/news.php
 * @link           http://codex.wordpress.org/Theme_Development#Widgets_.28sidebar.php.29
 * @since          available since Release 1.0
 */
?>  
<?php get_header(); ?>
<div class="banner"><h2>RDA News</h2></div>
<div class="news grid col-940">
	<div class="grid col-140 sidebar">
		<?php wp_nav_menu( array('menu' => 'News Menu', 'theme_location' => 'news' )); ?>
	</div>
	<div class="grid col-540 main">
		<h3>Latest News</h3>
		<?php
				$args = array( 'post_type' => 'News Entries', 'posts_per_page' => 5 );
				$loop = new WP_Query( $args );
				while ( $loop->have_posts() ) : $loop->the_post();
					$title = get_the_title();
					$post_date = get_the_date();
					echo "<section class='news-entry'>";
					echo "<h3 class='news-entry-title'>$title</h3>";
					echo "<p class='news-entry-date'>$post_date</p>";
					echo '<div class="news-entry-content">';
					the_content();
					echo '</div>';
					echo '</section>';
				endwhile;
		?>
	</div>
	<div class="grid col-140 fit">
		<h5>Latest Gallery</h5>
		<h5>Latest Video</h5>
	</div>
</div>
<?php get_footer(); ?>